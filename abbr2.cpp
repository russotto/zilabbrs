// Z machine abbreviation finder.
// Copyright 2021 Matthew T. Russotto.
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#include <compare>
#include <functional>
#include <iostream>
#include <iterator>
#include <stack>
#include <string>
#include <tuple>
#include <vector>

#include "gesa.hpp"

// Converts UCS-2 to UTF8.  II must be an input iterator to 16-bit characters,
// IO an output iterator to 8-bit characters.
template <typename II, typename IO>
void to_utf8(II start, II end, IO out) {
  uint16_t ch;
  while (start != end) {
    ch = (uint16_t)(*start++);
    if (ch < 0x80) {
      *out++ = ch;
    } else if (ch < 0x800) {
      *out++ = ((0xc0 | (ch >> 6)) & 0xFF);
      *out++ = ((0x80 | (ch & 0x3F)) & 0xFF);
    } else {
      *out++ = ((0xe0 | (ch >> 12)) & 0xFF);
      *out++ = ((0x80 | ((ch >> 6) & 0x3F)) & 0xFF);
      *out++ = ((0x80 | (ch & 0x3F)) & 0xFF);
    }
  }
}

// Converts UTF8 to UCS2.  II must be an input iterator to 8-bit characters, IO
// an output iterator to 16-bit characters.  Characters outside UCS2 are
// dropped. No error checking.
template <typename II, typename IO>
void to_ucs2(II start, II end, IO out) {
  uint8_t ch;
  while (start != end) {
    ch = (uint16_t)(*start++);
    if (ch < 0x80) {
      *out++ = ch;
    } else if ((ch & 0xE0) == 0xC0) {
      uint16_t outch;
      outch = ((ch & 0x1F) << 6);
      assert(start != end);
      ch = (uint16_t)(*start++);
      outch |= (ch & 0x3F);
      *out++ = outch;
    } else if ((ch & 0xF0) == 0xE0) {
      uint16_t outch;
      outch = ((ch & 0xF) << 12);
      assert(start != end);
      ch = (uint16_t)(*start++);
      outch |= ((ch & 0x3F) << 6);
      assert(start != end);
      ch = (uint16_t)(*start++);
      outch |= (ch & 0x3F);
      *out++ = outch;
    }
  }
}

std::ostream& operator<<(std::ostream& o, const std::u16string& u16) {
  std::ostream_iterator<char> out_it(o);
  to_utf8(u16.begin(), u16.end(), out_it);
  return o;
}

std::ostream& operator<<(std::ostream& o, const std::basic_string_view<char16_t>& u16) {
  std::ostream_iterator<char> out_it(o);
  to_utf8(u16.begin(), u16.end(), out_it);
  return o;
}

constexpr char startMark[] = "****RAWSTRINGS****\n";
constexpr char endMark[] = "****END RAWSTRINGS****\n";

typedef int32_t index_t;
typedef std::u16string String;
typedef std::basic_string_view<String::value_type> StringView;
constexpr String::value_type kSentinel = 0;
typedef gesa::GESA<index_t, String::value_type> GESA;

std::vector<String> getStrings(char* fname) {
  constexpr int bufsize = std::max(sizeof(startMark), sizeof(endMark)) + 1;
  char buf[bufsize];
  FILE* f = fopen(fname, "rb");
  if (f == NULL) {
    perror("Couldn't open string file");
    exit(-1);
  }
  bool gotStart = false;
  off_t startMarkOff;
  off_t endMarkOff;
  char* line = fgets(buf, bufsize, f);
  while (line) {
    if (!gotStart) {
      if (!strcmp(startMark, buf)) {
        startMarkOff = endMarkOff = ftello(f);
        gotStart = true;
      }
    } else {
      if (!strcmp(endMark, buf)) {
        break;
      }
      endMarkOff = ftello(f);
    }
    line = fgets(buf, bufsize, f);
  }
  if (!gotStart) {
    fprintf(stderr, "No start marker %s in file %s\n", startMark, fname);
    exit(-1);
  }
  if (!line) {
    fprintf(stderr, "No end marker %s in file %s\n", endMark, fname);
    exit(-1);
  }
  int strsize = endMarkOff - startMarkOff - 1;  // -1 skips the last newline
  char* strbuf = (char*)malloc(strsize);
  fseeko(f, startMarkOff, SEEK_SET);
  fread(strbuf, 1, strsize, f);
  fclose(f);
  char* s = strbuf;
  char* endbuf = strbuf + strsize;
  std::vector<String> result;
  while (s < endbuf) {
    size_t len = strlen(s);
    to_ucs2(s, s + len, std::back_inserter(result.emplace_back()));
    s += len + 1;
  }
  free(strbuf);
  return result;
}

int ZCharCost(String::value_type ch) {
  int cost;
  if ((ch >= 'a' && ch <= 'z') || ch == ' ') {
    // Alphabet A0 and space.
    cost = 1;
  } else if (ch >= 'A' && ch <= 'Z') {
    // Alphabet A1
    cost = 2;  // SHIFT + char.
  } else if ((ch >= '0' && ch <= '9') ||
             (strchr("\n.,!?_#'\"/\\-:()", ch) != NULL)) {
    // Alphabet A2
    cost = 2;  // SHIFT + char.
  } else {
    if (ch == '\r') {
      fprintf(stderr, "Carriage return in text\n");
      exit(-1);
    }
    cost = 4;  // ZSCII escape.
  }
  return cost;
}

typedef uint32_t zcost_t;
typedef std::vector<zcost_t> ZCostVector;

zcost_t calcZCostByIndex(const ZCostVector& costVector, index_t start, index_t length) {
  return costVector[start + length - 1] - (start?costVector[start - 1]:0);
}

ZCostVector calcZCosts(String str) {
  ZCostVector zc;
  zc.reserve(str.size());
  zcost_t accum = 0;
  for (auto ch : str) {
    accum += ZCharCost(ch);
    zc.push_back(accum);
  }
  return zc;
}

ZCostVector calcZCostsMulti(String str) {
  ZCostVector zc;
  zc.reserve(str.size());
  zcost_t accum = 0;
  for (auto ch : str) {
    if (ch == kSentinel) {
      accum = 0;
    } else {
      accum += ZCharCost(ch);
    }
    zc.push_back(accum);
  }
  return zc;
}

int ZStringCost(String str) {
  int cost = 0;
  for (auto ch : str) {
    cost += ZCharCost(ch);
  }
  return cost;
}

void printAbbr(const std::string& abbr, int strno, int count, int savings) {
  printf("        .FSTR FSTR?%d,\"", strno);
  for (auto ch : abbr) {
    putc(ch, stdout);
    if (ch == '"') putc(ch, stdout);
  }
  printf("\"\t\t; %dx, saved %d\n", count, savings);
}

void printAbbr(const std::u16string& abbr, int strno, int count, int savings) {
  printf("        .FSTR FSTR?%d,\"", strno);
  std::string abbr8;
  to_utf8(abbr.begin(), abbr.end(), std::back_inserter(abbr8));
  for (auto ch : abbr8) {
    putc(ch, stdout);
    if (ch == '"') putc(ch, stdout);
  }
  printf("\"\t\t; %dx, saved %d\n", count, savings);
}

void printWords(int nabbr) {
  printf("WORDS::\n");
  for (int i = 0; i < nabbr; i++) {
    printf("        FSTR?%d\n", i + 1);
  }
  printf("        .ENDI\n");
}

int count_overlap(const std::vector<index_t>* occp, int abbrlength) {
  int removed = 0;
  if (occp->empty()) return removed;
  auto mark = occp->begin();
  auto candidate = mark;
  candidate++;
  while (candidate != occp->end()) {
    if ((*mark + abbrlength) > *candidate) {
      removed++;
    } else {
      mark = candidate;
    }
    candidate++;
  }
  return removed;
}

std::vector<index_t> remove_overlap(const std::vector<index_t>& occp,
                                    int abbrlength,
                                    index_t* largest_truncated) {
  std::vector<index_t> result;
  *largest_truncated = 0;
  if (occp.empty()) return result;
  auto mark = occp.begin();
  result.push_back(*mark);
  auto candidate = mark;
  candidate++;
  while (candidate != occp.end()) {
    if ((*mark + abbrlength) <= *candidate) {
      mark = candidate;
      result.push_back(*mark);
    } else {
      *largest_truncated = std::max(*largest_truncated, *candidate - *mark);
    }
    candidate++;
  }
  return result;
}

struct annotated_occurrence_t {
  annotated_occurrence_t() = default;
  annotated_occurrence_t(index_t in_pos, index_t in_len, index_t in_rank)
      : first(in_pos), second(in_len), rank(in_rank) {}
  union {  // position
    index_t first;
    index_t pos;
  };
  union {  // length
    index_t second;
    index_t length;
  };
  index_t rank;
  bool operator<(const annotated_occurrence_t& other) const {
    return this->pos < other.pos;
  }
};

// Rounds to a multiple of N.
int round_to_multiple(int number, int N) {
  return ((number + N - 1) / N) * N;
}

int calc_abbr_savings(int count, int cost) {
  // The cost of the copy of the abbreviation in the abbreviation table is
  // always rounded.
  return count * (cost - 2) - round_to_multiple(cost, 3);
}

struct candidate_t {
  candidate_t() = default;
  candidate_t(const GESA::Interval& in_interval, index_t in_length,
              index_t in_count, index_t in_cost)
      : interval(in_interval),
        length(in_length),
        count(in_count),
        cost(in_cost) {
    calc_savings();
    score = savings;
  }
  GESA::Interval interval;
  index_t length;
  index_t count;
  int cost;
  int savings;
  int score = 0;
  int id;
  bool stored_occurrences = false;
  bool operator<(const struct candidate_t& other) const {
    if (score == other.score) return count > other.count;
    return score < other.score;
  }
  bool operator>(const struct candidate_t& other) const {
    if (score == other.score) return count < other.count;
    return score > other.score;
  }
  void calc_savings() { savings = calc_abbr_savings(count, cost); }
  void set_count(int new_count) {
    count = new_count;
    calc_savings();
  }
  void set_cost(int new_cost) {
    cost = new_cost;
    calc_savings();
  }
};

std::vector<annotated_occurrence_t> remove_new_overlap(
    const std::vector<annotated_occurrence_t>& old,
    const std::vector<annotated_occurrence_t>& nova, index_t* largest_truncated,
    bool* fully_overlapped) {
  std::vector<annotated_occurrence_t> result;
  *largest_truncated = 0;
  *fully_overlapped = false;
  if (nova.empty()) {
    return result;
  }
  if (old.empty()) {
    result = nova;
    return result;
  }
  result.reserve(nova.size());
  auto olditer = old.begin();
  auto newiter = nova.begin();
  while (olditer != old.end() && newiter != nova.end()) {
    if (olditer->first < newiter->first) {
      if (olditer->first + olditer->second > newiter->first)
        newiter++;
      else {
        annotated_occurrence_t to_find(newiter->first, 0, 0);
        olditer =
            std::upper_bound(++olditer, old.end(), to_find,
                             [](const annotated_occurrence_t& a,
                                const annotated_occurrence_t& b) {
                               return a.first + a.second < b.first + b.second;
                             });
      }
    } else {
      if (newiter->first + newiter->second <= olditer->first)
        result.push_back(*newiter);
      else {
        *largest_truncated =
            std::max(*largest_truncated, olditer->first - newiter->first);
        if (olditer->first == newiter->first &&
            olditer->first + olditer->second ==
                newiter->first + newiter->second) {
          // Duplicate.
          // This sometimes happens when we split an edge.  Reject this without
          // marking it as fully overlapped.
          assert(result.empty());
          return result;
        }
        if (olditer->first + olditer->second <=
            newiter->first + newiter->second) {
          // Full overlap -- this abbreviation will not appear
          *fully_overlapped = true;
          assert(result.empty());
          return result;
        }
      }
      newiter++;
    }
  }
  while (newiter != nova.end()) {
    result.push_back(*newiter++);
  }
  return result;
}

std::vector<index_t> getSortedOccurrences(const GESA& SA,
                                          const candidate_t& candidate) {
  auto occ = SA.getOccurrences(candidate.interval);
  std::vector<index_t> result(occ.begin(), occ.end());
  std::sort(result.begin(), result.end());
  return result;
}

bool split_edge(const GESA& SA, ZCostVector& costArray, index_t split_point,
                candidate_t* to_split) {
  if (split_point > 0) {
    index_t parent_path_length = SA.parentPathLength(to_split->interval);
    // if the parent path length is greater than or equal to the proposed split
    // point, the edge from the parent node to the candidate node doesn't
    // contain the split point, so there's nothing to do.
    if (parent_path_length < split_point) {
      to_split->length = split_point;
      index_t pos = SA.getOccurrence(to_split->interval);
      auto cost =
          costArray[pos - 1 + split_point] - (pos ? costArray[pos - 1] : 0);
      to_split->set_cost(cost);
      if (to_split->savings > 0) {
        //              String rrep = combstring.substr(SA[to_split->left],
        //              largest_truncated); std::cerr << "Split pushing " <<
        //              rrep << " len " << largest_truncated << " ppl " <<
        //              parent_path_length <<  " " << cost << " " <<
        //              to_split.count << " " << to_split.savings << std::endl;
        return true;
      }
    }
  }
  return false;
}

class StopWatch {
 public:
  StopWatch() { timerclear(&elapsedTime); }
  StopWatch(bool startTimer) : StopWatch() {
    if (startTimer) start();
  }

  void start() {
    if (!running) {
      numStarts++;
      running = true;
      gettimeofday(&startTime, NULL);
    }
  }
  void stop() {
    if (running) {
      running = false;
      updateTime();
    }
  }
  struct timeval elapsed() {
    if (running) {
      updateTime();
    }
    return elapsedTime;
  }
  int starts() { return numStarts; }

 private:
  void updateTime() {
    struct timeval tmp;
    gettimeofday(&tmp, NULL);
    timersub(&tmp, &startTime, &tmp);
    timeradd(&tmp, &elapsedTime, &elapsedTime);
    timeradd(&tmp, &startTime, &startTime);
  }

  struct timeval startTime;
  struct timeval elapsedTime;
  int numStarts = 0;
  bool running = false;
};

constexpr int kMaxAbbrs = 96;
class abbrBitSet {
 public:

  abbrBitSet& set(std::size_t pos) {
    assert((pos >> 6) < bytesize);
    bits[pos >> 6] |= (uint64_t(1) << (pos&63));
    return *this;
  }

  abbrBitSet& clear(std::size_t pos) {
    assert((pos >> 6) < bytesize);
    bits[pos >> 6] &= ~(uint64_t(1) << (pos&63));
    return *this;
  }

  size_t next(std::size_t pos) const {
    assert((pos >> 6) < bytesize);
    pos++;
    size_t byte = pos >> 6;
    if (byte >= bytesize) return SIZE_MAX;
    int bit = int(pos)&63;
    uint64_t tmp = bits[byte] & ~((uint64_t(1) << bit) - 1);
    while (!tmp && ++byte < bytesize) {
      tmp = bits[byte];
    }
    if (tmp)
      return (byte << 6) | __builtin_ctzl(tmp);
    return SIZE_MAX;
  }

  size_t first() const {
    if (bits[0] & 1) return 0;
    return next(0);
  }

  constexpr bool operator[](std::size_t pos) const {
    return bits[pos >> 6] & (uint64_t(1) << (pos&63));
  }
 private:
  static constexpr size_t bytesize = (kMaxAbbrs+63)/64;
  uint64_t bits[bytesize] = {0};
};

int rescore(const GESA& SA,
            const std::vector<abbrBitSet>& abbrsAt,
            std::vector<candidate_t>& best_candidates,
            const ZCostVector& costArray, index_t* counts,
            bool round3,
            StopWatch* part1) {
  const int abbrRefCost = 2;
  int savings = 0;
  part1->start();
  for (int i = 0; i < best_candidates.size(); i++) {
    // Always round the cost regardless of the round3 setting.  This cost depends only
    // on the abbreviation length and not on the other abbreviations.
    savings -= round_to_multiple(best_candidates[i].cost, 3);
    if (counts) {
      counts[i] = 0;
    }
  }
  part1->stop();
  const auto& text = SA.getText();
  const auto& bounds = SA.getTextBounds();
  auto lbi = bounds.begin();
  auto ubi = lbi + 1;
  std::vector<int> minRemainingCost;
  std::vector<int> bestAbbr;
  for (;lbi != bounds.end(); lbi = ubi, ubi++) {
    int pos = *lbi;
    // The extra -1 is because the sentinel is in the text.
    int length = ((ubi == bounds.end())?text.size():*ubi) - pos - 1;
    minRemainingCost.resize(length + 1);
    bestAbbr.resize(length);
    minRemainingCost[length] = 0;
    for (int i = length - 1; i >=0 ; i--) {
      int charCost = calcZCostByIndex(costArray, pos + i, 1);
      minRemainingCost[i] = charCost + minRemainingCost[i + 1];
      bestAbbr[i] = -1;
      const auto& abbrsHere = abbrsAt[pos + i];
      for (size_t abbrNo = abbrsHere.first(); abbrNo != SIZE_MAX; abbrNo = abbrsHere.next(abbrNo)) {
        int abbrLen = best_candidates[abbrNo].length;
        int costWithAbbr = abbrRefCost + minRemainingCost[i + abbrLen];
        if (costWithAbbr < minRemainingCost[i]) {
          minRemainingCost[i] = costWithAbbr;
          bestAbbr[i] = abbrNo;
        }
      }
    }
    int oldcost = calcZCostByIndex(costArray, pos, length);
    int oldrawcost = oldcost;
    int newcost = minRemainingCost[0];
    int newrawcost = newcost;
    if (round3) {
      oldcost = round_to_multiple(oldcost, 3);
      newcost = round_to_multiple(newcost, 3);
    }
    savings += oldcost - newcost;
    bool printed = false;
    if (counts) {
      for (int i = 0; i < length; ) {
        if (bestAbbr[i] == -1) i++;
        else {
          counts[bestAbbr[i]]++;
          i += best_candidates[bestAbbr[i]].length;
        }
      }
    }
  }
  return savings;
}

void usage(char* pname) {
    fprintf(stderr, "Usage: %s [+ar] <file-with-strings>\n", pname);
    exit(-1);
}

void set_occurrences(const GESA& SA, candidate_t* candidate,
                     std::vector<abbrBitSet>* abbrsAt, int index, bool setToTrue,
                     StopWatch* timer) {
  timer->start();
  const auto& occurrences = SA.getOccurrences(candidate->interval);
  if (setToTrue) {
    for (int occ : occurrences) {
      (*abbrsAt)[occ].set(index);
    }
  } else {
    for (int occ : occurrences) {
      (*abbrsAt)[occ].clear(index);
    }
  }
  timer->stop();
}

void store_occurrences(const GESA& SA, candidate_t* candidate,
                       std::vector<std::vector<index_t>>* abbrsAt, StopWatch* timer)
{
  timer->start();
  const auto& occurrences = SA.getOccurrences(candidate->interval);
  for (int occ : occurrences) {
    (*abbrsAt)[occ].push_back(candidate->id);
  }
  candidate->stored_occurrences = true;
  timer->stop();
}

int main(int argc, char* argv[]) {
  bool do_rounding = true;
  bool do_affix_refinement = true;
  bool do_reverse_tree = true;
  int optind = 1;
  while (optind < argc) {
    if (argv[optind][0] == '+' || argv[optind][0] == '-') {
      bool turn_on = argv[optind][0] == '-';
      for (char *avp = &argv[optind][1]; *avp != 0; avp++) {
        switch (*avp) {
          case 'r':
            do_rounding = turn_on;
            break;
          case 'R':
            do_reverse_tree = turn_on;
            break;
          case 'a':
            do_affix_refinement = turn_on;
            break;
          default:
            usage(argv[0]);
        }
      }
    } else break;
    optind++;
  }
  if (optind >= argc) {
    usage(argv[0]);
  }
  char* fname = argv[optind];
  auto strings = getStrings(fname);
#if 0
    int i = 0;
    for (const auto& s:strings) {
        printf("%d |%.20s|\n", i++, s.c_str());
    }
#endif
  printf("        ; Frequent words file for %s\n\n", fname);
  int niters = 0;
  int abbrno;
  StopWatch buildArrayTime(true);
  GESA SA(strings);
  auto costArray = calcZCostsMulti(SA.getText());
  buildArrayTime.stop();
  std::vector<candidate_t> contestants;
  StopWatch traverseTime(true);
  StopWatch rTraverseTime;
  StopWatch rBuildArrayTime;
  int nextId = 0;
  SA.traverseBottomUp([&contestants, &SA, &nextId, do_reverse_tree,
                       &costArray](const GESA::IntervalWithLength& interval) {
    index_t count;
    count = interval.rb - interval.lb + 1;
    index_t pos = SA.getOccurrence(interval);
    auto cost = calcZCostByIndex(costArray, pos, interval.lcp);
#if 0
    fprintf(stderr, "%d %d %d count: %d cost: %d savings: %d\n", interval.lb, interval.rb, interval.lcp, count, cost, cost * (count - 2 ) - cost);
    auto ss1 = SA.representative(interval);
    std::cerr << " \"" << String(ss1) << "\"" <<  std::endl;
    //                           fprintf(stderr, " \"%.*s\"\n", interval.lcp, combstring.data() + SA[interval.rb]);
#endif
//#define ALL_ABBRS
#ifdef ALL_ABBRS
    for (int i = SA.parentPathLength(interval) + 1; i <= interval.lcp; i++) {
      contestants.emplace_back(interval, i, count, cost);
      if (contestants.back().savings <= 0) contestants.pop_back();
      else contestants.back().id = nextId++;
    }
#else
    contestants.emplace_back(interval, interval.lcp, count, cost);
    if (contestants.back().savings <= 0) contestants.pop_back();
    else {
      contestants.back().id = nextId++;
      if (!do_reverse_tree) {
        // If the candidate ends with a space, and the version without a space
        // isn't in the suffix tree, also add the version without a space.
        // This may allow for a less-overlapped candidate.
        if (SA.parentPathLength(interval) + 1 < interval.lcp &&
            SA.representative(interval).back() == ' ') {
          // The cost of a space is 1.
          contestants.emplace_back(interval, interval.lcp - 1, count, cost - 1);
          if (contestants.back().savings <= 0) contestants.pop_back();
          else contestants.back().id = nextId++;
        }
      }
    }
#endif
  });
  traverseTime.stop();
  if (do_reverse_tree) {
    rBuildArrayTime.start();
    std::vector<String> rstrings(strings.size());
    for (int i = 0; i < rstrings.size(); i++) {
      rstrings[i].reserve(strings[i].size());
      std::reverse_copy(strings[i].begin(), strings[i].end(), std::back_inserter(rstrings[i]));
    }
    GESA RSA(rstrings);
    rBuildArrayTime.stop();
    rTraverseTime.start();
    RSA.traverseBottomUp([&contestants, &RSA, &SA, &nextId,
                          &costArray](const GESA::IntervalWithLength& rinterval) {
        String rep = String(RSA.representative(rinterval));
        std::reverse(rep.begin(), rep.end());
        auto interval = SA.findSAInterval(rep);
        index_t pos = SA.getOccurrence(interval);
        if (SA.distanceToNode(interval) == 0)
          return;
        index_t count;
        count = interval.rb - interval.lb + 1;
        auto cost = calcZCostByIndex(costArray, pos, interval.lcp);
        contestants.emplace_back(interval, interval.lcp, count, cost);
        if (contestants.back().savings <= 0) contestants.pop_back();
        else contestants.back().id = nextId++;
      });
  }
  rTraverseTime.stop();
  std::cerr << "Num contestants " << contestants.size() << std::endl;

  std::vector<candidate_t> best_candidates;
  index_t counts[kMaxAbbrs];
  std::vector<candidate_t>& runner_heap = contestants;
  StopWatch heapTime(true);
  std::make_heap(runner_heap.begin(), runner_heap.end());
  heapTime.stop();
  StopWatch occTime;
  StopWatch copyTime;
  StopWatch rescoreTime;
  StopWatch rescoreMarkTime;
  StopWatch replAffixTime;
  std::vector<abbrBitSet> abbrsAt(SA.getText().size());
  int rej = 0;
  int prevTotSavings = 0;
  while (abbrno < kMaxAbbrs && !runner_heap.empty()) {
    std::pop_heap(runner_heap.begin(), runner_heap.end());
    auto& runnerup = *runner_heap.rbegin();
    set_occurrences(SA, &runnerup, &abbrsAt, best_candidates.size(), true, &occTime);
    copyTime.start();
    best_candidates.emplace_back(runnerup);
    copyTime.stop();
    rescoreTime.start();
    int newTotSavings = rescore(SA, abbrsAt, best_candidates, costArray,
                                best_candidates.size() == kMaxAbbrs?counts:nullptr,
                                false, // round3
                                &rescoreMarkTime);
    rescoreTime.stop();
    int increase = newTotSavings - prevTotSavings;
    if (increase < runner_heap.front().score) {
      best_candidates.pop_back();
      set_occurrences(SA, &runnerup, &abbrsAt, best_candidates.size(), false, &occTime);
      runnerup.score = increase;
      std::push_heap(runner_heap.begin(), runner_heap.end());
    } else {
      abbrno++;
      prevTotSavings = newTotSavings;
      runner_heap.pop_back();
    }
  }
  bool any_replaced = true;
  int passes = 0;
  bool now_rounding = false;
  int minsavings = prevTotSavings;  // a convenient big number
  for (int i = 0; i < best_candidates.size(); i++) {
    const auto& candidate = best_candidates[i];
    int savings = calc_abbr_savings(counts[i], candidate.cost);
    if (savings < minsavings) minsavings = savings;
  }
  while (do_affix_refinement && any_replaced && passes < 5) {
    passes++;
    //    std::cerr << "Pass: " << passes << " " << runner_heap.size() << std::endl;
    any_replaced = false;
    replAffixTime.start();
    std::vector<candidate_t> discard_heap;
    while (!runner_heap.empty()) {
      std::pop_heap(runner_heap.begin(), runner_heap.end());
      auto& runnerup = runner_heap.back();
      if (runnerup.savings < minsavings - 3) {
        // This candidate probably won't work, period.
        runner_heap.pop_back();
        continue;
      }
      bool replaced = false;
      for (int i = 0; i < best_candidates.size() && !replaced; i++) {
        auto bcrep = SA.representative(best_candidates[i].interval, best_candidates[i].length);
        auto rrep = SA.representative(runnerup.interval, runnerup.length);
        int bccost = best_candidates[i].cost;
        int bcsavings = calc_abbr_savings(counts[i], bccost);
        bool longerok = bcsavings < runnerup.savings;
        if (bcrep.starts_with(rrep) || bcrep.ends_with(rrep) ||
            (longerok && (rrep.starts_with(bcrep) || rrep.ends_with(bcrep)))) {
          using std::swap;
          swap(best_candidates[i], runnerup);
          set_occurrences(SA, &runnerup, &abbrsAt, i, false, &occTime);
          set_occurrences(SA, &best_candidates[i], &abbrsAt, i, true, &occTime);
          int newTotSavings = rescore(SA, abbrsAt, best_candidates, costArray,
                                      counts, now_rounding, &rescoreMarkTime);
          int increase = newTotSavings - prevTotSavings;
          if (increase > 0) {
            best_candidates[i].score = increase;
            replaced = true;
            any_replaced = true;
            //            std::cerr << "replacing '" << bcrep << "'  with '" << rrep << "' (" << increase
            //                      << ", " << prevTotSavings << ", " << newTotSavings << ")" << std::endl;
            //            std::cerr << "newTotSavings " << newTotSavings << " pass " << passes << std::endl;
            prevTotSavings = newTotSavings;
          } else {
            //            std::cerr << "not replacing '" << bcrep << "'  with '" << rrep << "' (" << increase
            //<< ", " << prevTotSavings << ", " << newTotSavings << ")" << std::endl;
            swap(best_candidates[i], runnerup);
            set_occurrences(SA, &runnerup, &abbrsAt, i, false, &occTime);
            set_occurrences(SA, &best_candidates[i], &abbrsAt, i, true, &occTime);
          }
        }
      }
      discard_heap.push_back(runnerup);
      runner_heap.pop_back();
    }
    if (!any_replaced && do_rounding && !now_rounding) {
      any_replaced = true;
      now_rounding = true;
      // Rescore with rounding.
      prevTotSavings = rescore(SA, abbrsAt, best_candidates, costArray,
                               counts,
                               now_rounding,
                               &rescoreMarkTime);
    }
    if (any_replaced) {
      runner_heap = std::move(discard_heap);
      std::make_heap(runner_heap.begin(), runner_heap.end());
    }
    replAffixTime.stop();
  }

  int tot_abbrs = std::min((int)best_candidates.size(), kMaxAbbrs);
  for (int i = 0; i < tot_abbrs; i++) {
    auto& candidate = best_candidates[i];
    String rrep(SA.representative(candidate.interval, candidate.length));
    candidate.set_count(counts[i]);
    printAbbr(rrep, i + 1, candidate.count, candidate.savings);
  }
  printWords(tot_abbrs);
#define PRINTTIME(sw)                                                         \
  do {                                                                        \
    auto x = (sw).elapsed();                                                  \
    fprintf(stderr, "%20s: %f %4d\n", #sw,                                    \
            (double)x.tv_sec + (double)x.tv_usec / 1000000.0, (sw).starts()); \
  } while (0)

  PRINTTIME(buildArrayTime);
  PRINTTIME(traverseTime);
  if (do_reverse_tree) {
    PRINTTIME(rBuildArrayTime);
    PRINTTIME(rTraverseTime);
  }
  PRINTTIME(heapTime);
  PRINTTIME(occTime);
  PRINTTIME(rescoreTime);
  PRINTTIME(rescoreMarkTime);
  PRINTTIME(replAffixTime);
  std::cerr << "Saved " << prevTotSavings << " of " << SA.getText().size() <<
      " text compression ratio: " << (100.0 * (SA.getText().size() - prevTotSavings)
                                      / (double)SA.getText().size()) << "%" << std::endl;
}
