// GESA: Generalized Enhanced Suffix Array
// Copyright 2021 Matthew T. Russotto.
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef GESA_H_
#define GESA_H_
#include <assert.h>
#include <functional>
#include <stack>
#include <span>
#include <string>
#include <string_view>
#include <vector>

#include <sais.hxx>

namespace gesa {
// Generalized Enhanced Suffix Array.
// TODO: make "bounds" optional
// TODO: top-down traversal.

template <typename ALFA_T, bool MAXSENTINEL> class GESASentinel {
};

template <typename ALFA_T> class GESASentinel<ALFA_T, true> {
 public:
  static constexpr ALFA_T kSentinel = std::numeric_limits<ALFA_T>::max();
};

template <typename ALFA_T> class GESASentinel<ALFA_T, false> {
 public:
  static constexpr ALFA_T kSentinel = std::numeric_limits<ALFA_T>::min();
};

template <typename INDEX_T, typename ALFA_T, bool MAXSENTINEL=false> class GESA :
      public GESASentinel<ALFA_T, MAXSENTINEL> {
 public:
  typedef std::basic_string<ALFA_T> String;
  typedef std::basic_string_view<ALFA_T> StringView;

  struct Interval {
    Interval() : lb(-1), rb(-1) {}
    Interval(INDEX_T lb_in, INDEX_T rb_in) : lb(lb_in), rb(rb_in) {}
    INDEX_T lb, rb;
    bool operator==(const Interval& other) const {
      return lb == other.lb && rb == other.rb;
    }
  };

  struct IntervalWithLength : Interval {
    IntervalWithLength(INDEX_T lcp_in, INDEX_T lb_in, INDEX_T rb_in) : Interval(lb_in, rb_in), lcp(lcp_in) {}
    INDEX_T lcp;
    bool operator==(const IntervalWithLength& other) const {
      return lcp == other.lcp &&
          (*static_cast<const Interval*>(this)) == static_cast<const Interval&>(other);
    }
  };

  GESA(std::vector<String> src) {
    static_assert(std::is_unsigned<ALFA_T>::value, "Alphabet type must be unsigned");
    for (const auto& s:src) {
      Bounds.push_back(INDEX_T(Text.size()));
      std::copy(s.data(), s.data() + s.size(), std::back_inserter(Text));
      Text.push_back(this->kSentinel);
    }
    build();
  }

#if 0
  GESA(std::vector<std::basic_string<std::make_signed<ALFA_T>::type>> src) {
    for (const auto& s:src) {
      std::copy((ALFA_T *)s.data(), (ALFA_T *)s.data() + s.size(), std::back_inserter(Text));
      Text.push_back(this->kSentinel);
    }
    build();
  }
#endif

  // Returns string index, char index of occurrence.
  std::pair<INDEX_T, INDEX_T> occurrenceToIndices(INDEX_T occ) {
    auto iter = std::upper_bound(Bounds.begin(), Bounds.end(), occ);
    --iter;
    return {iter - Bounds.begin(), occ - *iter};
  }

  // Bottom-up traversal of SA/LCP, M.I. Abouelhoda et al (2004)
  // Does not traverse the leaves.
  void traverseBottomUp(std::function<void(const IntervalWithLength&)> report) {
    //    std::stack<IntervalWithLength, std::vector<IntervalWithLength>> nodestack;
    std::stack<IntervalWithLength> nodestack;
    nodestack.emplace(0,0,LCP.size()); // this is never going to be popped.
    for (INDEX_T i = 1; i < LCP.size(); i++) {
      INDEX_T lb = i - 1;
      while (LCP[i] < nodestack.top().lcp) {
        nodestack.top().rb = i -1;
        lb = nodestack.top().lb;
        report(nodestack.top());
        nodestack.pop();
      }
      if (LCP[i] > nodestack.top().lcp) {
        nodestack.emplace(LCP[i], lb, LCP.size());
      }
    }
    //  fprintf(stderr, "Finishing up\n");
    while (0 < nodestack.top().lcp) {
      nodestack.top().rb = LCP.size() -1;
      report(nodestack.top());
      nodestack.pop();
    }
  }

  std::span<const INDEX_T> getOccurrences(const Interval& ii) const {
    return getOccurrences(ii.lb, ii.rb);
  }

  INDEX_T getOccurrence(const Interval& ii) const {
    return SA[ii.lb];
  }

  INDEX_T getOccurrence(int ii) const {
    return SA[ii];
  }

  std::span<const INDEX_T> getOccurrences(INDEX_T left, INDEX_T right) const {
    return std::span<const INDEX_T>(SA.begin() + left, SA.begin() + right + 1);
  }

  INDEX_T parentPathLength(const Interval& ii) const {
    return parentPathLength(ii.lb, ii.rb);
  }
  INDEX_T parentPathLength(INDEX_T left, INDEX_T right) const {
    return std::max(LCP[left], right == LCP.size() - 1? LCP[left]:LCP[right + 1]);
  }

  std::basic_string_view<ALFA_T> representative(const Interval& ii, INDEX_T length) const {
    return representative(ii.lb, length);
  }

  std::basic_string_view<ALFA_T> representative(const IntervalWithLength& ii) const {
    return representative(ii.lb, ii.lcp);
  }

  std::basic_string_view<ALFA_T> representative(INDEX_T left, INDEX_T length) const {
    return std::basic_string_view<ALFA_T>(&Text[SA[left]], length);
  }

  IntervalWithLength findSAInterval(StringView s) const {
    INDEX_T occ = findOccurrenceSAIndex(s);
    INDEX_T left = occ, right = occ;
    if (occ != SA.size()) {
      while (right + 1 < LCP.size() && LCP[right + 1] >= s.size()) {
        right++;
      }
      while (left > 0 && LCP[left] >= s.size()) {
        left--;
      }
    }
    return IntervalWithLength(s.size(), left, right);
  }

  // Tests if this interval represents a node in the syntax tree.  If it does, returns 0.
  // If it is not a node but is on an edge, returns the distance to the next node. (that is,
  // (ii.lcp + distance, ii.lb, ii.rb is a node).
  // Otherwise returns -1.
  INDEX_T distanceToNode(const IntervalWithLength& ii) const {
    if (ii.lb != 0 && LCP[ii.lb] >= ii.lcp) {
      // Not a node; more chars to the left
      return -1;
    }
    if (ii.rb + 1 < LCP.size() && LCP[ii.rb + 1] >= ii.lcp) {
      // Not a node; more chars to the right
      return -1;
    }
    INDEX_T result = SA.size();
    for (int i = ii.lb + 1 ; i <= ii.rb; i++ ) {
      result = std::min(result, LCP[i] - ii.lcp);
      // Not a node; chars in interval not identical to ii.lcp.
      if (result < 0) return -1;
    }
    return result;
  }

  std::vector<INDEX_T> findOccurrences(StringView s) const {
    INDEX_T occ = findOccurrenceSAIndex(s);
    if (occ != SA.size()) {
      INDEX_T right = occ;
      std::vector<INDEX_T> stdocc;
      stdocc.emplace_back(SA[occ]);
      while (right + 1 < LCP.size() && LCP[right + 1] >= s.size()) {
        right++;
          stdocc.emplace_back(SA[right]);
      }
      INDEX_T left = occ;
      while (left > 0 && LCP[left] >= s.size()) {
        left--;
          stdocc.emplace_back(SA[left]);
      }
    }
  }

  const String& getText() const {
    return Text;
  }
  const std::vector<INDEX_T>& getTextBounds() const {
    return Bounds;
  }

  void traverseTopDown(std::function<void(const IntervalWithLength&)> report,
                       bool report_leaves = false) {
    buildChildTable();
    // Top level special case -- first l-value is nextlIndex[0] == child[0];
    int left = 0;
    int right;
    do {
      right = Child[left];
      if (left != right - 1 || report_leaves)
        report(IntervalWithLength(getLCP(left, right - 1), left, right - 1));
      if (left != right - 1) {
        traverseTopDown(left, right - 1, report, report_leaves);
      }
      left = right;

    } while (right != SA.size());
  }

 private:
  std::vector<INDEX_T> SA;
  std::vector<INDEX_T> LCP;
  std::vector<INDEX_T> Bounds;
  std::vector<INDEX_T> Child;
  String Text;

  void build() {
    SA.resize(Text.size());
    auto res = saisxx(Text.data(), SA.data(), (INDEX_T)Text.size(),
                      (INDEX_T)((size_t)std::numeric_limits<ALFA_T>::max() -
                                (ssize_t)std::numeric_limits<ALFA_T>::min() + 1));
    if (res != 0)
      throw res;
    LCPPhi();
  }
  // Modified Kasai's algoritm, usually called "phi".
  // Due to J. Ka ̈rkka ̈inen, G. Manzini, and S. J. Puglisi
  // Further modified to treat a sentinel value as not matching anything, even itself.
  //
  // I haven't proved this works, but I believe it should, provided that either all the alphabet
  // values are greater than the sentinel, or all of them are less than the sentinel.
  // Reasoning:
  // In any adjacent run of suffixes which have no differences up to a sentinel value, the
  // ordering of those suffixes is the same as some ordering of suffixes that could exist if
  // the combined string had unique sentinels for all the substrings.  Thus, the LCP algorithm
  // will have to check the sentinel, and hit upon the special case.
  void LCPPhi() {
    const int n = SA.size();
    auto& phi = LCP; // Reuse phi array for result.
    phi.resize(n);
    std::vector<INDEX_T> plcp(n);
    for (INDEX_T i = 1; i < n; i++) {
      phi[SA[i]] = SA[i-1];
    }
    INDEX_T l = 0;
    for (INDEX_T i = 0; i < n -1; i++) {
      if (i == SA[0]) {
        plcp[i] = 0;
      }
      else {
        while(Text[i + l] != this->kSentinel && Text[i + l] == Text[phi[i] + l]) {
          l++;
        }
        plcp[i] = l;
      }
      if (l > 0) --l;
    }
    for (INDEX_T i = 0; i < n; i++) {
      LCP[i] = plcp[SA[i]];
    }
  }

  INDEX_T findOccurrenceSAIndex(const StringView& s) const {
    INDEX_T off = 0;
    INDEX_T low = 0;
    INDEX_T high = SA.size() - 1;
    while (low <= high) {
      typedef std::basic_string_view<ALFA_T> sv_t;
      INDEX_T mid = low + ((high - low) / 2);
      sv_t highview(Text.begin() + SA[high],
                    Text.begin() + std::min(Text.size(), SA[high] + s.size()));
      sv_t lowview(Text.begin() + SA[low],
                   Text.begin() + std::min(Text.size(), SA[low] + s.size()));
      while (off< highview.size() && off < lowview.size() && off < s.size() &&
             highview[off] == lowview[off] && highview[off] == s[off]) off++;
      sv_t midview(Text.begin() + SA[mid] + off,
                   Text.begin() + std::min(Text.size(), SA[mid] + s.size()));
      sv_t soff(s.begin() + off, s.end());
      auto comp = midview <=> soff;
      if (comp == std::weak_ordering::less) {
        low = mid + 1;
      } else if (comp == std::weak_ordering::greater) {
        high = mid - 1;
      } else {
      return mid;
      }
    }
    return SA.size();
  }

  // Build child table, as Algorithm 6.2 and 6.5 in Abouelhoda
  //
  void buildChildTable() {
    if (!Child.empty()) return;
    Child.resize(SA.size());
      // Algorithm 6.2 -- calculate up and down values.
    std::vector<INDEX_T> stk;
    INDEX_T lastIndex = -1;
    stk.push_back(0);
    for (INDEX_T i = 1; i <= SA.size(); i++) {
      INDEX_T LCP_i = (i == SA.size())?0:LCP[i];
      while (LCP_i < LCP[stk.back()]) {
        lastIndex = stk.back();
        stk.pop_back();
        if (LCP_i <= LCP[stk.back()] && LCP[stk.back()] != LCP[lastIndex]) {
          Child[stk.back()] = lastIndex;  // "down" field of Child[stk.back()]
          //          fprintf(stderr, "      Down %d = %d\n", stk.back(), lastIndex);
        }
      }
      assert(LCP_i >= LCP[stk.back()]);
      if (lastIndex != -1) {
        Child[i - 1] = lastIndex; // "up" field of Child[i];
        //        fprintf(stderr, "        Up %d = %d\n", i-1, lastIndex);
        lastIndex = -1;
      }
      stk.push_back(i);
    }
    stk.resize(0);

    // Algorithm 6.5, construct nextlIndex values
    stk.push_back(0);
    for (INDEX_T i = 1; i <= SA.size(); i++) {
      INDEX_T LCP_i = (i == SA.size())?0:LCP[i];
      while (LCP_i < LCP[stk.back()]) {
        stk.pop_back();
      }
      if (LCP_i == LCP[stk.back()]) {
        //        fprintf(stderr, "NextLIndex %d = %d\n", stk.back(), i);
        Child[stk.back()] = i;
        stk.pop_back();
      }
      stk.push_back(i);
    }
    assert(stk.size() == 1);
  }

  // Gets the "nextLIndex" value in the child table for this entry, if it has one.
  INDEX_T getNextlIndex(INDEX_T i) {
    if (LCP[i] > LCP[i + 1]) {
      //      fprintf(stderr, "next l index of %d is -1, contains up of %d: %d\n", i, i + 1, Child[i]);
      return -1;
    }
    if (LCP[Child[i]] == LCP[i]) {
      //      fprintf(stderr, "next l index of %d is %d\n", i, Child[i]);
      return Child[i];
    } else {
      //      fprintf(stderr, "next l index of %d is -1, contains down of %d: %d\n", i, i, Child[i]);
      return -1;
    }
  }

  // Gets the "down" value in the child table for this entry, if it has one.
  INDEX_T getDown(INDEX_T i) {
    if (LCP[Child[i]] == LCP[i]) {
      return Child[Child[i] - 1];
    } else {
      assert(LCP[Child[i]] >= LCP[i]);
      return Child[i];
    }
  }

  // [left, right] must be an "lcp interval", that is, the extent of a suffix tree node, or a leaf.
  INDEX_T getLCP(INDEX_T i, INDEX_T j) {
    assert(Child.size());
    if (i == j) {
      // this adds a factor of log(num strings) to leaf reporting.
      auto iter = std::upper_bound(Bounds.begin(), Bounds.end(), SA[i]);
      if (iter == Bounds.end())
        return Text.size() - SA[i] - 1;
      return *iter - SA[i] - 1;
    }
    INDEX_T up = Child[j];
    if (i < up && up <= j) return LCP[up];
    return LCP[getDown(i)];
  }

  void traverseTopDown(INDEX_T left, INDEX_T last_right,
                       std::function<void(const IntervalWithLength&)> report,
                       bool report_leaves) {
    INDEX_T right;
    auto init_right =
        [&]{
          assert(last_right + 1 == SA.size() || LCP[last_right] > LCP[last_right + 1]);
          if (left < Child[last_right] && Child[last_right] <= last_right)
            right = Child[last_right];  // Child[last_right + 1].up
          else
            right = getDown(left);
        };
    init_right();
    std::stack<INDEX_T, std::vector<INDEX_T>> stk;
    while(1) {
      while (left != last_right + 1) {
        if (right == -1)
          right = last_right + 1;
        if (left != right - 1 || report_leaves)
          report(IntervalWithLength(getLCP(left, right - 1), left, right - 1));
        INDEX_T next_right = getNextlIndex(right);
        if (left != right - 1) {
          // Iterative equivalent to :
          //        traverseTopDown(left, right - 1, report, report_leaves);
          stk.push(last_right);
          last_right = right - 1;
          init_right();
          continue;
        }
        left = right;
        right = getNextlIndex(left);
      }
      if (stk.empty()) break;
      left = last_right + 1;
      right = getNextlIndex(left);
      last_right = stk.top();
      stk.pop();
    }
  }
};

}  // namespace gesa

#endif // GESA_H_
