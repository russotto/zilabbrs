CC=gcc-mp-10
CXX=g++-mp-10
CXXFLAGS = --std=c++20 -g -O2 -I./sais-lite-2.4.1

ABBR2_OBJS = abbr2.o
all: abbr2

abbr2: $(ABBR2_OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $(ABBR2_OBJS)

clean:
	$(RM) -f $(ABBR2_OBJS)
